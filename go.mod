module code.gitea.io/tea

go 1.12

require (
	code.gitea.io/sdk/gitea v0.0.0-20191013013401-e41e9ea72caa
	github.com/go-gitea/yaml v0.0.0-20170812160011-eb3733d160e7
	github.com/mattn/go-runewidth v0.0.4 // indirect
	github.com/olekukonko/tablewriter v0.0.1
	github.com/stretchr/testify v1.3.0
	github.com/urfave/cli v1.20.0
	gopkg.in/src-d/go-git.v4 v4.13.1
	gopkg.in/yaml.v2 v2.2.2 // indirect
)
